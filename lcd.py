from __future__ import annotations

from typing import List

import dwf
from dataclasses import dataclass
import errors
import pin_defs
from memory_view import MemoryView
from enum import Enum
from ctypes import *


@dataclass
class LCDMessage:
    rs: bool
    rw: bool
    data: int

@dataclass
class LCDRawPacket:
    enable: bool
    rs: bool
    rw: bool
    data: int


class RAMType(Enum):
    DDRAM = 1
    CGRAM = 2


class CursorDirection(Enum):
    LEFT = 1
    RIGHT = 2


@dataclass
class LCDPointer:
    ram_type: RAMType = RAMType.DDRAM
    address: int = 0


class LCDInterpreter:
    def __init__(self):
        self.ddram = MemoryView(raise_oob_exception=False, on_oob_address=0)
        self.cgram = MemoryView(raise_oob_exception=False, on_oob_address=0)
        self.address_counter = LCDPointer()
        self.cursor_direction = CursorDirection.RIGHT
        self.display_shifting_on = False
        self.display_on = False
        self.cursor_blinking = False
        self.display_shift = 0
        self.data_length = 0
        self.display_lines = 0
        self.font = (0, 0)
        self.cursor_on = False

    def instruction_step(self, instruction: LCDMessage):
        # Reads data, nothing to do
        if instruction.rw:
            return

        if instruction.rs:
            if self.address_counter.ram_type == RAMType.DDRAM:
                self.ddram.add_data(self.address_counter.address, [instruction.data & 0b0111_1111])
            elif self.address_counter.ram_type == RAMType.CGRAM:
                self.ddram.add_data(self.address_counter.address, [instruction.data & 0b0011_1111])
            self.address_counter.address += 1 if self.cursor_direction == CursorDirection.RIGHT else -1
            return

        bits = list(map(lambda bit: True if bit == '1' else False, format(instruction.data, 'b')))
        while len(bits) < 8:
            bits.append(False)
        bits = list(reversed(bits))

        clear_display_mask = 0b1111_1111
        clear_display_match = 0b0000_0001

        return_home_mask = 0b1111_1110
        return_home_match = 0b0000_0010

        entry_mode_set_mask = 0b1111_1100
        entry_mode_set_match = 0b0000_0100

        display_control_mask = 0b1111_1000
        display_control_match = 0b0000_1000

        shift_control_mask = 0b1111_0000
        shift_control_match = 0b0001_0000

        function_set_mask = 0b1110_0000
        function_set_match = 0b0010_0000

        set_cgram_mask = 0b1100_0000
        set_cgram_match = 0b0100_0000

        set_ddram_mask = 0b1000_0000
        set_ddram_match = 0b0000_0000

        if instruction.data & clear_display_mask == clear_display_match:
            self.ddram.clear()
            self.address_counter.address = 0
            self.address_counter.ram_type = RAMType.DDRAM
        elif instruction.data & return_home_mask == return_home_match:
            self.address_counter.address = 0
            self.address_counter.ram_type = RAMType.DDRAM
            self.display_shift = 0
        elif instruction.data & entry_mode_set_mask == entry_mode_set_match:
            self.display_shifting_on = bits[0]
            self.cursor_direction = CursorDirection.RIGHT if bits[1] else CursorDirection.LEFT
        elif instruction.data & display_control_mask == display_control_match:
            self.display_on = bits[2]
            self.cursor_on = bits[1]
            self.cursor_blinking = bits[0]
        elif instruction.data & shift_control_mask == shift_control_match:
            if bits[3]:
                self.display_shift += 1 if bits[2] else -1
            else:
                self.address_counter.address += 1 if bits[2] else -1
        elif instruction.data & function_set_mask == function_set_match:
            self.data_length = 8 if bits[4] else 4
            self.display_lines = 2 if bits[3] else 1
            self.font = (5, 10) if bits[2] else (5, 8)
        elif instruction.data & set_cgram_mask == set_cgram_match:
            self.address_counter.address = instruction.data & 0b0011_1111
            self.address_counter.ram_type = RAMType.CGRAM
        elif instruction.data & set_ddram_mask == set_ddram_match:
            self.address_counter.address = instruction.data & 0b0111_1111
            self.address_counter.ram_type = RAMType.DDRAM

        self.display_shift %= 40

    def run_instructions(self, instructions: List[LCDMessage]):
        for message in instructions:
            self.instruction_step(message)

    def get_line_string(self, line: int) -> str:
        if line < 1 or line > 2:
            raise AttributeError
        line_str = ""
        for i in range(16):
            i = (i + self.display_shift) % 40
            i += 0 if line == 1 else 40
            character = self.ddram[i]
            # If null, it displays as a space
            if character == 0:
                character = 32
            line_str += chr(character)
        return line_str



class LCDSpyDevice:
    def __init__(self, device: dwf.Dwf, rs_channel: int | None = None, rw_channel: int | None = None,
                 enable_channel: int | None = None, d4_channel: int | None = None, d5_channel: int | None = None,
                 d6_channel: int | None = None, d7_channel: int | None = None, frequency: int = 100_000):
        self.digilent_device = dwf.DwfDigitalIn(device)
        if rs_channel is None:
            rs_channel = pin_defs.LCD_RS_PIN
        if rw_channel is None:
            rw_channel = pin_defs.LCD_RW_PIN
        if enable_channel is None:
            enable_channel = pin_defs.LCD_ENABLE_PIN
        if d4_channel is None:
            d4_channel = pin_defs.LCD_D4_PIN
        if d5_channel is None:
            d5_channel = pin_defs.LCD_D5_PIN
        if d6_channel is None:
            d6_channel = pin_defs.LCD_D6_PIN
        if d7_channel is None:
            d7_channel = pin_defs.LCD_D7_PIN
        self.rs_channel = rs_channel
        self.rw_channel = rw_channel
        self.enable_channel = enable_channel
        self.d4_channel = d4_channel
        self.d5_channel = d5_channel
        self.d6_channel = d6_channel
        self.d7_channel = d7_channel
        self.spying = False

    def start_spying(self):
        if self.spying:
            raise errors.DeviceAlreadySpyingError
        print(f"Starting spying on channels e: {self.enable_channel}, rs: {self.rs_channel}, rw: {self.rw_channel}")
        print(f"Data channels(d7-d4): {self.d7_channel} {self.d6_channel} {self.d5_channel} {self.d4_channel}")
        self.spying = True
        self.digilent_device.acquisitionModeSet(self.digilent_device.ACQMODE.RECORD)
        self.digilent_device.dividerSet(-1)
        self.digilent_device.sampleFormatSet(16)
        self.digilent_device.triggerPositionSet(1000)
        # Note, this is accessing the raw API since the class library lacks it
        hdwf = self.digilent_device.hdwf
        dwf.dwfdll.FDwfDigitalInTriggerResetSet(hdwf, c_int(0), c_int(0), c_int(1 << self.enable_channel), c_int(0))
        dwf.dwfdll.FDwfDigitalInTriggerSet(hdwf, c_int(0), c_int(1 << self.enable_channel), c_int(0), c_int(0))
        # Min - 0.9ms, max - 1.5ms
        dwf.dwfdll.FDwfDigitalInTriggerLengthSet(hdwf, c_double(0.0005), c_double(0.0015), c_int(0))
        dwf.dwfdll.FDwfDigitalInTriggerCountSet(hdwf, c_int(1), c_int(0))

        self.digilent_device.configure(False, True)
        # Clear any data which might have been caputed previously
        self.get_data_raw()

    def extract(self, data: int) -> LCDRawPacket:
        e = bool(data & (1 << self.enable_channel))
        rs = bool(data & (1 << self.rs_channel))
        rw = bool(data & (1 << self.rw_channel))
        d4 = 1 if data & (1 << self.d4_channel) else 0
        d5 = 1 if data & (1 << self.d5_channel) else 0
        d6 = 1 if data & (1 << self.d6_channel) else 0
        d7 = 1 if data & (1 << self.d7_channel) else 0
        nibble = d4 | d5 << 1 | d6 << 2 | d7 << 3
        return LCDRawPacket(enable=e, rs=rs, rw=rw, data=nibble)

    def process_data(self, data: [int]) -> List[LCDRawPacket]:
        extract = lambda x: self.extract(x)
        return list(map(extract, data))

    def get_data_raw(self) -> List[int]:
        status = self.digilent_device.status(True)
        available, lost, corrupt = self.digilent_device.statusRecord()
        return self.digilent_device.statusData(available)

    def finish_spying(self) -> List[LCDRawPacket]:
        if not self.spying:
            raise errors.DeviceNotSpyingError
        self.spying = False
        data = self.get_data_raw()
        return self.process_data(data)
    
    def reset(self):
        self.spying = False


def packets_to_messages(packets: List[LCDRawPacket]) -> List[LCDMessage]:
    messages = []
    while len(packets) > 2:
        high, low, *packets = packets
        if high.rs != low.rs or high.rw != low.rw:
            raise errors.InvalidDataError
        data = high.data << 4 | low.data
        message = LCDMessage(rs=high.rs, rw=high.rw, data=data)
        messages.append(message)
    return messages
