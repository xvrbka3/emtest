from emtest import EmTest
import time


def test_gpio_interrupts_used(emtest: EmTest):
    emtest.session.target.halt()
    assert emtest.interrupts.gpio.has_any_interrupt_set()


def test_nothing_printed(emtest: EmTest):
    time.sleep(2)
    data_ready = emtest.serial.in_waiting
    time.sleep(30)
    assert emtest.serial.in_waiting == data_ready


def test_servo_orientation(emtest: EmTest):
    time.sleep(5)
    emtest.serial.flushInput()
    assert emtest.serial.in_waiting == 0
    emtest.servo.set_servo_angle(0)
    time.sleep(10)
    assert emtest.serial.in_waiting != 0
    previous_serial = emtest.serial.in_waiting
    emtest.servo.set_servo_angle(90)
    time.sleep(5)
    assert emtest.serial.in_waiting != previous_serial
