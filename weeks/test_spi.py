from emtest import EmTest
import time
import re

from memory_view import MemoryView
from spi_device import SPIMessage

CONTROL_MEASURE_SAMPLING_ADDRESS = 106
TEMPERATURE_OVERSAMPLING_MASK = 0b11100000
PRESSURE_OVERSAMPLING_MASK = 0b00011100


def test_correct_initialization(emtest: EmTest):
    emtest.session.target.reset_and_halt()
    emtest.serial.reset_input_buffer()
    emtest.spi.start_spying()
    time.sleep(0.1)
    emtest.session.target.resume()
    time.sleep(10)
    messages = emtest.spi.finish_spying()
    emtest.serial.read(emtest.serial.in_waiting)

    # You are probably not sending any messages
    assert len(messages) != 0

    mem_view = MemoryView(raise_oob_exception=False, on_oob_address=0)
    for message in messages:
        mem_view.add_data_from_packet(message)

    sampling_config = mem_view[116]
    # sampling_config & TEMPERATURE_OVERSAMPLING_MASK
    assert sampling_config & TEMPERATURE_OVERSAMPLING_MASK
    # sampling_config & PRESSURE_OVERSAMPLING_MASK
    assert sampling_config & PRESSURE_OVERSAMPLING_MASK


def test_correct_values_printed(emtest: EmTest):
    time.sleep(2)

    emtest.serial.reset_input_buffer()
    emtest.spi.start_spying()
    time.sleep(10)
    messages = emtest.spi.finish_spying()
    debug_output = str(emtest.serial.read(emtest.serial.in_waiting))

    # No messages were sent
    assert len(messages) != 0

    debug_printed_numbers = list(map(lambda x: int(x), re.findall('[0-9]+', debug_output)))

    def sensor_data_filter(spi_message: SPIMessage) -> bool:
        return not spi_message.write and spi_message.address == 119 and len(spi_message.mosi_data) == 6

    for message in filter(sensor_data_filter, messages):
        uncompressed_pres = message.miso_data[0] << 12 | message.miso_data[1] << 4 | message.miso_data[2] >> 4
        uncompressed_temp = message.miso_data[3] << 12 | message.miso_data[4] << 4 | message.miso_data[5] >> 4
        assert uncompressed_pres in debug_printed_numbers
        assert uncompressed_temp in debug_printed_numbers
