from typing import List, Tuple

from emtest import EmTest
import socket
import time
import os
from errors import NoIpSetError


def get_ip_address() -> str:
    # Note: Set this in config.toml of the gitlab-runner
    env_ip = os.getenv("MCU_LINKLOCAL_IP")
    if os.getenv("CI") == "true" and env_ip is None:
        raise NoIpSetError
    if env_ip is not None:
        return env_ip
    hostname = socket.gethostname()
    address = socket.gethostbyname(hostname)
    return address


def generate_replacement_files() -> List[Tuple[str, str]]:
    ip_address = get_ip_address()
    a0, a1, a2, a3 = ip_address.split('.')
    address_file_string = """#pragma once
#define configSER_ADDR0 {}
#define configSER_ADDR1 {}
#define configSER_ADDR2 {}
#define configSER_ADDR3 {}

#define DESTINATION_PORT 8089""".format(a0, a1, a2, a3)
    return [('source/address.h', address_file_string)]


class TestNetwork(EmTest):

    def setup_udp_socket(self):
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_socket.bind(('0.0.0.0', 8089))
        self.udp_socket.setblocking(False)

    def setup_class(self):
        self.generated_replacements = generate_replacement_files()
        self.to_delete = ['doc', 'lwip/doc']
        self.ip_address = get_ip_address()
        super().setup_class(self)
        self.setup_udp_socket(self)

    def teardown_class(self):
        super().teardown_class(self)
        self.udp_socket.close()

    def test_any_data_received(self):
        self.session.target.reset()
        time.sleep(1)
        self.buttons.sw2.on()
        self.buttons.sw3.on()
        time.sleep(1)
        self.buttons.sw2.off()
        self.buttons.sw3.off()
        time.sleep(10)
        # This will raise an error if no message was received, that is fine, test will fail
        (msg, addr) = self.udp_socket.recvfrom(4096)
        assert 'value=' in str(msg)
