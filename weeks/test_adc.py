import math
import random
import time
from typing import List

import pytest

from emtest import EmTest


def sin_waveform(samples: int = 32) -> List[float]:
    return [math.cos((i / samples) * 2 * math.pi) * 0.5 + 0.5 for i in range(samples)]


def _set_and_test_values(emtest: EmTest, red: float, green: float):
    emtest.analog.output[0].set_value(green)
    emtest.analog.output[1].set_value(red)
    time.sleep(0.2)
    assert emtest.leds.green() == pytest.approx(green, abs=0.25)
    assert emtest.leds.red() == pytest.approx(red, abs=0.25)


def test_x_coordinate(emtest: EmTest):
    waveform = sin_waveform(32)
    emtest.analog.output[1].set_value(0)
    for i in waveform:
        emtest.analog.output[0].set_value(i)
        time.sleep(0.2)
        assert emtest.leds.green() == pytest.approx(i, abs=0.25)


def test_y_coordinate(emtest: EmTest):
    waveform = sin_waveform(32)
    emtest.analog.output[0].set_value(0)
    for i in waveform:
        emtest.analog.output[1].set_value(i)
        time.sleep(0.2)
        assert emtest.leds.red() == pytest.approx(i, abs=0.25)


def test_both_coordinates(emtest: EmTest):
    waveform = sin_waveform(64)
    for i in waveform:
        _set_and_test_values(emtest, i, 1 - i)


def test_random_values(emtest: EmTest):
    random.seed(42)
    for _ in range(64):
        green = random.uniform(0.0, 1.0)
        red = random.uniform(0.0, 1.0)
        _set_and_test_values(emtest, red, green)
