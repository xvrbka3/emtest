from typing import List

import pytest

from emtest import EmTest
import time
from lcd import LCDInterpreter
from lcd import LCDRawPacket
import lcd


def seek_start(packets: List[LCDRawPacket]):
    # Note: if this raised an error, your submission is possibly wrong
    first, second, third, fourth = packets[0], packets[1], packets[2], packets[3]
    while first.data != 0b0011 or second.data != 0b0011 or third.data != 0b0011 or fourth.data != 0b0010:
        _, *packets = packets
        first, second, third, fourth = packets[0], packets[1], packets[2], packets[3]


@pytest.mark.parametrize("emtest", [True], indirect=True)
def test_proper_initialization(emtest: EmTest):
    """
    Proper initialization basically checks if the LCD is initialized as the library students should use
    does it, there are actually more ways to do it and some actions can be done in different order
    """
    emtest.session.target.reset_and_halt()
    emtest.lcd.start_spying()
    emtest.session.target.resume()
    time.sleep(1)
    packets = emtest.lcd.finish_spying()
    for packet in packets:
        print(packet)
    seek_start(packets)
    messages = lcd.packets_to_messages(packets)
    assert len(messages) >= 5
    # Initial startup commands
    for message in messages[:5]:
        assert message.rs is False
        assert message.rw is False
    # Two startup commands
    assert messages[0].data == 0b0011_0011
    assert messages[1].data == 0b0011_0010
    # Interface data length command
    assert (messages[2].data & 0b1111_1100) == 0b0010_1100
    # Set display on, cursor and blinking off
    assert messages[3].data == 0b0000_1100
    # Clear display
    assert messages[4].data == 0b0000_0001


@pytest.mark.parametrize("emtest", [True], indirect=True)
def test_any_text_sent(emtest: EmTest):
    emtest.session.target.reset_and_halt()
    emtest.lcd.start_spying()
    emtest.session.target.resume()
    time.sleep(5)
    packets = emtest.lcd.finish_spying()
    seek_start(packets)
    messages = lcd.packets_to_messages(packets)
    for message in messages:
        print(message)
    lcd_sim = LCDInterpreter()
    lcd_sim.run_instructions(messages)
    line1 = lcd_sim.get_line_string(1).strip()
    line2 = lcd_sim.get_line_string(2).strip()
    assert len(line1) != 0 or len(line2) != 0
