from typing import Tuple

import pytest

from emtest import EmTest
import colorsys
import time


def rgb_to_hsv_normalized(rgb: Tuple[float, float, float]) -> Tuple[int, int, int]:
    r, g, b = rgb
    h, s, v = colorsys.rgb_to_hsv(r, g, b)
    return int(h * 256), int(s * 256), int(v * 256)


def test_hsv_cycle(emtest: EmTest):
    """
    Effectively, we check if the next RGB value corresponds to higher hue with some rollover protection
    it will need live testing, and we expect that we will need to make adjustments
    """
    # Chosen by testing certain amount of values
    sample_count = 4_000
    blue_hue_value = 165
    time.sleep(0.5)
    prev_hsv = rgb_to_hsv_normalized(emtest.leds.get_led_values(sample_count))
    rollovers = 0
    for _ in range(500):
        next_hsv = rgb_to_hsv_normalized(emtest.leds.get_led_values(sample_count))
        prev_h, _, _ = prev_hsv
        next_h, _, _ = next_hsv
        # Rollover protection
        if next_h <= 80 and prev_h > next_h and prev_h >= blue_hue_value:
            rollovers += 1
        else:
            assert prev_h - 80 <= next_h
        prev_hsv = next_hsv
    # We expect at least four rollovers
    assert rollovers >= 4

@pytest.mark.skip(reason="interrupt use not mandatory")
def test_interrupts_used(emtest: EmTest):
    assert emtest.interrupts.pit.has_any_interrupt_set()
