from typing import Tuple

from emtest import EmTest
import pytest
import time
import random


def color_approx_equal(sampled_color: float, tested_color: float, abs_tol: float) -> bool:
    return abs(sampled_color - tested_color) < abs_tol


def rgb_color_test(sampled_colors: Tuple[float, float, float], tested_colors: Tuple[float, float, float], abs_tol: float) -> bool:
    red_test = color_approx_equal(sampled_colors[0], tested_colors[0], abs_tol)
    green_test = color_approx_equal(sampled_colors[1], tested_colors[1], abs_tol)
    blue_test = color_approx_equal(sampled_colors[2], tested_colors[2], abs_tol)
    print("Sampled: " + str(sampled_colors) + " tested: " + str(tested_colors) + "results: " + str((red_test, green_test, blue_test)))
    return red_test and green_test and blue_test


def test_full_light(emtest: EmTest):
    full_light_data = bytes([255, 255, 255])
    time.sleep(0.5)
    emtest.uart.write(full_light_data)
    emtest.uart.flush()
    assert emtest.leds.red() == pytest.approx(1.0, abs=0.2)
    assert emtest.leds.green() == pytest.approx(1.0, abs=0.2)
    assert emtest.leds.blue() == pytest.approx(1.0, abs=0.2)


def test_random_data(emtest: EmTest):
    time.sleep(0.5)
    random.seed(42)
    failed_pwm = 0
    iterations = 50
    for _ in range(iterations):
        red = random.randint(0, 255)
        green = random.randint(0, 255)
        blue = random.randint(0, 255)
        packet = bytes([red, green, blue])
        emtest.uart.write(packet)
        emtest.uart.flush()
        time.sleep(0.5)
        red_pwm, green_pwm, blue_pwm = emtest.leds.get_led_values()
        red = red / 255
        green = green / 255
        blue = blue / 255
        if not rgb_color_test((red_pwm, green_pwm, blue_pwm), (red, green, blue), 0.2):
            failed_pwm += 1
    # Note: This means most pwm samples deviated too much from the sent values
    assert (failed_pwm / iterations) <= 0.1
