from typing import Callable
from emtest import EmTest
from buttons import press
from leds import Leds
import pytest
import time
import random


class RGBStateMachine:

    def __init__(self):
        self.red = False
        self.green = False
        self.blue = False
        self.index = 0

    def next(self):
        self.index += 1
        self.index %= 3

    def switch(self):
        if self.index == 0:
            self.red = not self.red
        elif self.index == 1:
            self.green = not self.green
        elif self.index == 2:
            self.blue = not self.blue

    def test(self, leds: Leds):
        assert leds.red() == pytest.approx(1.0 if self.red else 0.0)
        assert leds.green() == pytest.approx(1.0 if self.green else 0.0)
        assert leds.blue() == pytest.approx(1.0 if self.blue else 0.0)


def led_switch(emtest: EmTest, led_val: Callable):
    assert led_val() == pytest.approx(0.0)
    press(emtest.buttons.sw3)
    assert led_val() == pytest.approx(1.0)
    time.sleep(0.5)
    press(emtest.buttons.sw3)
    assert led_val() == pytest.approx(0.0)


def test_red_switching(emtest: EmTest):
    led_switch(emtest, emtest.leds.red)


def test_green_switching(emtest: EmTest):
    press(emtest.buttons.sw2)
    led_switch(emtest, emtest.leds.green)


def test_blue_switching(emtest: EmTest):
    press(emtest.buttons.sw2)
    time.sleep(0.5)
    press(emtest.buttons.sw2)
    led_switch(emtest, emtest.leds.blue)


def test_button_response_only_once(emtest: EmTest):
    emtest.buttons.sw3.on()
    time.sleep(1)
    emtest.buttons.sw3.off()
    assert emtest.leds.red() == pytest.approx(1.0)
    time.sleep(0.5)
    emtest.buttons.sw3.on()
    time.sleep(1.3)
    emtest.buttons.sw3.off()
    assert emtest.leds.red() == pytest.approx(0.0)
    time.sleep(0.5)
    emtest.buttons.sw3.on()
    time.sleep(1.5)
    emtest.buttons.sw3.off()
    assert emtest.leds.red() == pytest.approx(1.0)


@pytest.mark.skip(reason="debouncing removal")
def test_switch_pulse_detection(emtest: EmTest):
    emtest.buttons.sw3.pulse()
    time.sleep(0.5)
    assert emtest.leds.red() == pytest.approx(0.0)



@pytest.mark.skip(reason="debouncing removal")
def test_next_pulse_detection(emtest: EmTest):
    emtest.buttons.sw2.pulse()
    time.sleep(0.5)
    emtest.buttons.sw2.reset()
    led_switch(emtest.leds.red)


def state_machine_iteration(emtest: EmTest, state_machine: RGBStateMachine):
    action_choice = random.randrange(0, 4)
    state_machine.test(emtest.leds)
    if action_choice == 0:
        emtest.buttons.sw3.on()
        time.sleep(0.5)
        emtest.buttons.sw3.off()
        state_machine.switch()
    elif action_choice == 1:
        emtest.buttons.sw2.on()
        time.sleep(0.5)
        emtest.buttons.sw2.off()
        state_machine.next()
    # elif action_choice == 2:
    #    emtest.buttons.sw3.pulse()
    #    time.sleep(0.5)
    #    emtest.buttons.sw3.reset()
    # elif action_choice == 3:
    #    emtest.buttons.sw2.pulse()
    #    time.sleep(0.5)
    #    emtest.buttons.sw2.reset()


def test_random_state_testing(emtest: EmTest):
    state_machine = RGBStateMachine()
    random.seed(42)
    for _ in range(50):
        state_machine_iteration(emtest, state_machine)
