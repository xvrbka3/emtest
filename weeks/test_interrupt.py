from emtest import EmTest
import time
import vtable_constants


def test_nothing_printed_on_idle(emtest: EmTest):
    emtest.buttons.sw3.off()
    time.sleep(5)
    data = emtest.serial.read(emtest.serial.in_waiting)
    assert len(data) == 0


def test_full_press(emtest: EmTest):
    emtest.buttons.sw3.off()
    time.sleep(1)
    emtest.buttons.sw3.on()
    time.sleep(1)
    data = emtest.serial.read(emtest.serial.in_waiting)
    assert len(data) != 0


def test_pulse(emtest: EmTest):
    emtest.buttons.sw3.off()
    time.sleep(1)
    emtest.buttons.sw3.pulse()
    time.sleep(0.5)
    data = emtest.serial.read(emtest.serial.in_waiting)
    assert len(data) == 0


def test_interrupts_used(emtest: EmTest):
    emtest.session.target.halt()
    assert emtest.interrupts.pit.has_any_interrupt_set()
    assert emtest.interrupts.gpio.interrupt_not_default(vtable_constants.PORT_A_VECTOR)
