from __future__ import annotations

from abc import ABC, abstractmethod
from typing import List

import dwf
from enum import Enum
from errors import SamplingInvariantError


class IDigitalOutPin(ABC):

    @abstractmethod
    def value(self) -> bool:
        """
        Gets an immediate value on the pin
        """
        pass

    @abstractmethod
    def on(self):
        """
        Sets the pin on
        """
        pass

    @abstractmethod
    def off(self):
        """
        Sets the pin off
        """
        pass

    @abstractmethod
    def pulse(self):
        """
        Runs a pulse on the pin which should not trigger if the user performs debouncig
        """
        pass

    @abstractmethod
    def reset(self):
        """
        Resets pin into default value
        """
        pass

    def __call__(self, *args, **kwargs) -> bool:
        return self.value()


class IDigitalInPin(ABC):

    @abstractmethod
    def value(self) -> float:
        """
        Gets pin value which is aproximated over many samples, in order to detect value set via PWM
        :return: Value between 0 and 1, 0 being 0 duty cycle, 1 being full duty cycle
        """
        pass

    def __call__(self, *args, **kwargs) -> float:
        return self.value()


class DigilentDigitalOutPin(IDigitalOutPin):
    def __init__(self, device: dwf.Dwf, channel: int, inverted: bool = False):
        self.channel = channel
        self.digilent_device = dwf.DwfDigitalOut(device)
        self.inverted = inverted
        self.digilent_device.enableSet(self.channel, True)
        self.digilent_device.counterInitSet(self.channel, self.inverted, 1)
        hz_sys = self.digilent_device.internalClockInfo()
        # 4kHz
        self.clock_div = int(hz_sys / 4000)
        self.digilent_device.dividerSet(self.channel, self.clock_div)
        self.clock_rate = 4000
        self.set_value(self.inverted)

    def value(self) -> bool:
        return self.digilent_device.outputGet(self.channel)

    def set_value(self, val: bool):
        self.digilent_device.counterSet(self.channel, 0 if val else 1, 1 if val else 0)
        self.digilent_device.configure(True)

    def on(self):
        self.set_value(not self.inverted)

    def off(self):
        self.set_value(self.inverted)

    def pwm_ticks(self, low: int | None = None, high: int | None = None):
        if low is None or high is None:
            raise ValueError
        if low is None:
            low = self.clock_rate - high
        if high is None:
            high = self.clock_rate - low
        self.digilent_device.configure(False)
        self.digilent_device.counterSet(self.channel, int(low), int(high))
        self.digilent_device.configure(True)

    def pulse(self):
        # Sets up a PWM with a small duty cycle
        pulse_length = 2
        low = pulse_length if self.inverted else self.clock_rate - pulse_length
        high = self.clock_rate - pulse_length if self.inverted else pulse_length
        self.pwm_ticks(low, high)

    def reset(self):
        self.set_value(self.inverted)


class SamplingState(Enum):
    UNPREPARED = 0
    RATE_SET = 1
    SAMPLING = 2


class DigilentDigitalInPin(IDigitalInPin):

    def __init__(self, device: dwf.Dwf, channel: int, inverted: bool = False):
        self.channel = channel
        self.digilent_device = dwf.DwfDigitalIn(device)
        self.inverted = inverted
        self.digilent_device.dividerSet(10_000)
        bus_size = 16
        self.digilent_device.sampleFormatSet(bus_size)
        self.sampling_state = SamplingState.UNPREPARED

    def single_value(self) -> bool:
        io_context = dwf.DwfDigitalIO(self.digilent_device)
        data = io_context.inputStatus()
        data &= (1 << self.channel)
        value = bool(data)
        return self.inverted ^ value

    def value(self, samples: int = 4_000) -> float:
        self.prepare_sampling(samples)
        self.start_sampling()
        data = self.get_data_blocking(samples)
        true_values = 0
        mask = (1 << self.channel)
        for sample in map(lambda x: x & mask, data):
            if sample != 0:
                true_values += 1
        value = true_values / samples
        if self.inverted:
            value = 1.0 - value
        return value

    def prepare_sampling(self, samples: int):
        if self.sampling_state is SamplingState.SAMPLING:
            raise SamplingInvariantError
        self.digilent_device.bufferSizeSet(samples)
        self.sampling_state = SamplingState.RATE_SET

    def start_sampling(self):
        if self.sampling_state is not SamplingState.RATE_SET:
            raise SamplingInvariantError
        self.digilent_device.configure(False, True)
        self.sampling_state = SamplingState.SAMPLING

    def start_group_sampling(self):
        self.start_sampling()

    def get_data_blocking(self, samples: int) -> List[int]:
        if self.sampling_state is not SamplingState.SAMPLING:
            raise SamplingInvariantError
        while self.digilent_device.status(True) != self.digilent_device.STATE.DONE:
            pass
        self.sampling_state = SamplingState.RATE_SET
        return self.digilent_device.statusData(samples)

    def get_data_blocking_group(self, samples) -> List[int]:
        return self.get_data_blocking(samples)
