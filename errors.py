class ClassInvariantError(BaseException):
    pass


class DeviceAlreadySpyingError(BaseException):
    pass


class DeviceNotSpyingError(BaseException):
    pass


class SamplingInvariantError(BaseException):
    pass


class InvalidDataError(BaseException):
    pass


class NoIpSetError(BaseException):
    pass
