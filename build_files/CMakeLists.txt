cmake_minimum_required(VERSION 3.5)

project(StudentTest CXX C ASM)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

set(LINKER_FILE ${CMAKE_CURRENT_LIST_DIR}/Linker_template_Debug.ld)

set(COMPILE_OPTIONS -DDEBUG
                    -DCPU_MK66FN2M0VMD18
                    -DPRINTF_ADVANCED_ENABLE
                    -DSDK_I2C_BASED_COMPONENT_USED=1
                    -DFRDM_K66F
                    -DSERIAL_PORT_TYPE_UART=1
                    -O0
                    -fno-common
                    -g3
                    -ffunction-sections
                    -fdata-sections
                    -ffreestanding
                    -fno-builtin
                    -fno-rtti
                    -fno-exceptions
                    -mcpu=cortex-m4
                    -mfpu=fpv4-sp-d16
                    -mfloat-abi=hard
                    -mthumb
                    -fstack-usage
                    -MMD
                    -MP)

set(LINKER_OPTIONS  -g3
                    --specs=nano.specs
                    --specs=nosys.specs
                    -fno-common
                    -ffunction-sections
                    -fdata-sections
                    -ffreestanding
                    -fno-builtin
                    -mapcs
                    -Xlinker
                    -static
                    -Xlinker
                    -D__STARTUP_CLEAR_BSS
                    -Xlinker
                    -z
                    -Xlinker
                    muldefs
                    -mcpu=cortex-m4
                    -mfpu=fpv4-sp-d16
                    -mfloat-abi=hard
                    -mthumb
                    -T ${LINKER_FILE}
                    -static)

add_compile_definitions(ARM_MATH_CM4;ARM_MATH_MATRIX_CHECK;ARM_MATH_ROUNDING)
#add_compile_options(-mfloat-abi=softfp -mfpu=fpv4-sp-d16)
#add_link_options(-mfloat-abi=softfp -mfpu=fpv4-sp-d16 --specs=nano.specs --specs=nosys.specs)
#add_link_options(-ffunction-sections -fdata-sections -ffreestanding -fno-builtin)

#add_compile_options(-mcpu=cortex-m4 -mthumb -mthumb-interwork)
#add_compile_options(-ffunction-sections -fdata-sections -fmessage-length=0)
#add_compile_options(-ffreestanding -fsigned-char -fshort-enums -fno-common)
#add_compile_options(-)

#add_compile_options(-DCPU_MK66FN2M0VMD18 -DPRINTF_ADVANCED_ENABLE=1 -DSDK_I2C_BASED_COMPONENT_USED=1)
#add_compile_options(-DFRDM_K66F -DFREEDOM -D__STARTUP_CLEAR_BSS -D__STARTUP_INITIALIZE_NONCACHEDATA)

#add_compile_options(-Og -g)

add_compile_options(${COMPILE_OPTIONS})
add_link_options(${LINKER_OPTIONS})

# https://gist.github.com/ronald112/77d9d1bbf0710f7b099a481f92063829

MACRO(SUBDIRLIST result firstdir curdir)
    file(GLOB ENDF6_SRC_TOP RELATIVE
        ${curdir} ${curdir}/*)
    file(GLOB_RECURSE ENDF6_SRC_NESTED ${curdir}/*)
    set(children ${ENDF6_SRC_TOP} ${ENDF6_SRC_NESTED})

    SET(dirlist "${firstdir}")
    FOREACH(child ${children})
        IF(IS_DIRECTORY ${curdir}/${child})
            LIST(APPEND dirlist ${curdir}/${child})
        ENDIF()
    ENDFOREACH()
    SET(${result} ${dirlist})
ENDMACRO()

SET(DIRECTORIES accel board CMSIS device drivers source src startup utilities lwip/src/include lwip/port
                lwip/contrib/apps/udpecho_raw BMP280_driver phy mdio)

SUBDIRLIST(COMPONENTS ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/component)

MESSAGE(${COMPONENTS})

include_directories(${DIRECTORIES} ${COMPONENTS} board/boards)


file(GLOB_RECURSE SOURCES "accel/*.c" "board/*.c" "CMSIS/*.c" "device/*.c" "drivers/*.c" "component/*.c" "source/*.c"
                          "startup/*.c" "utilities/*.c" "accel/*.cpp" "board/*.cpp" "CMSIS/*.cpp" "device/*.cpp"
                          "drivers/*.cpp" "component/*.cpp" "source/*.cpp" "startup/*.cpp" "utilities/*.cpp" "accel/*.s"
                          "board/*.s" "CMSIS/*.s" "device/*.s" "drivers/*.s" "component/*.s" "source/*.s" "startup/*.s"
                          "utilities/*.s" "BMP280_driver/*.c" "*.c" "lwip/src/core/*.c"
                          "lwip/src/core/ipv4/*.c" "lwip/src/core/ipv6/*.c" "lwip/src/api/*.c" "lwip/src/netif/*.c"
                          "lwip/src/net/ppp/*.c" "lwip/src/net/ppp/polarssl/*.c" "lwip/port/*.c" "phy/*.c" "mdio/*.c")

add_executable(${PROJECT_NAME}.elf ${SOURCES})

TARGET_LINK_LIBRARIES(${PROJECT_NAME}.elf -Wl,--start-group)
target_link_libraries(${PROJECT_NAME}.elf debug m)
target_link_libraries(${PROJECT_NAME}.elf debug c)
target_link_libraries(${PROJECT_NAME}.elf debug gcc)
target_link_libraries(${PROJECT_NAME}.elf debug nosys)
TARGET_LINK_LIBRARIES(${PROJECT_NAME}.elf -Wl,--end-group)

set(HEX_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.hex)
set(BIN_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.bin)
set(ELF_FILE ${PROJECT_BINARY_DIR}/${PROJECT_NAME}.elf)

add_custom_command(TARGET ${PROJECT_NAME}.elf POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${PROJECT_NAME}.elf> ${HEX_FILE}
        COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${PROJECT_NAME}.elf> ${BIN_FILE}
        COMMENT "Building ${HEX_FILE}
Building ${BIN_FILE}")
