from typing import List, Tuple

import dwf

import pin_defs

from digital_pin import DigilentDigitalInPin


class Leds:
    def __init__(self, digilent_device: dwf.Dwf):
        self.red = DigilentDigitalInPin(digilent_device, pin_defs.RED_LED_PIN_POS, inverted=True)
        self.green = DigilentDigitalInPin(digilent_device, pin_defs.GREEN_LED_PIN_POS, inverted=True)
        self.blue = DigilentDigitalInPin(digilent_device, pin_defs.BLUE_LED_PIN_POS, inverted=True)

    def process_group_sample_data(self, data: List[int]) -> Tuple[int, int, int]:
        red_mask = (1 << self.red.channel)
        green_mask = (1 << self.green.channel)
        blue_mask = (1 << self.blue.channel)
        red_pwm, green_pwm, blue_pwm = (0, 0, 0)
        for (red, green, blue) in map(lambda x: (x & red_mask, x & green_mask, x & blue_mask), data):
            if red != 0:
                red_pwm += 1
            if green != 0:
                green_pwm += 1
            if blue != 0:
                blue_pwm += 1
        return red_pwm, green_pwm, blue_pwm

    def normalize(self, pwm_data: (int, int, int), sample_count: int) -> Tuple[float, float, float]:
        red_pwm, green_pwm, blue_pwm = pwm_data
        red_normalized = red_pwm / sample_count
        green_normalized = green_pwm / sample_count
        blue_normalized = blue_pwm / sample_count
        red_normalized = 1 - red_normalized if self.red.inverted else red_normalized
        green_normalized = 1 - green_normalized if self.green.inverted else green_normalized
        blue_normalized = 1 - blue_normalized if self.blue.inverted else blue_normalized
        return red_normalized, green_normalized, blue_normalized

    def get_led_values(self, samples: int = 4_000) -> Tuple[float, float, float]:
        self.red.prepare_sampling(samples)
        self.green.prepare_sampling(samples)
        self.blue.prepare_sampling(samples)
        self.red.start_group_sampling()
        data = self.red.get_data_blocking_group(samples)
        pwm_data = self.process_group_sample_data(data)
        return self.normalize(pwm_data, samples)
