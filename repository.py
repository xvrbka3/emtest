from git import Repo


def download_repo(repo_url: str, location: str) -> Repo:
    repo = Repo.clone_from(repo_url, location)
    return repo
