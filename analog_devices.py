import dwf

import pin_defs
from analog_pin import AnalogOutPin


class AnalogDevices:
    def __init__(self, digilent_device: dwf.Dwf):
        self.output = [AnalogOutPin(pin_defs.ANALOG_OUT_PIN_1, digilent_device),
                       AnalogOutPin(pin_defs.ANALOG_OUT_PIN_2, digilent_device)]
