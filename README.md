# EmTest

This project was created as part of the diploma thesis:
https://is.muni.cz/auth/th/m6ad7/

The steps required to run it are desctibed in said diploma thesis.

Tests themself can be found in the weeks folder.

## Known and possible issues

The board might stop responding with error message akin to "ACK not received" or similar.
We suspect that this error is caused due to a short, so if it happens again, message us to we can investigate it.
