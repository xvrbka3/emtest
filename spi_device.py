from __future__ import annotations

from dataclasses import dataclass
from typing import List, Tuple

import dwf

import pin_defs
from data_packet import IAddressablePacket
import errors


@dataclass
class SPIMessage(IAddressablePacket):
    write: bool
    address: int
    mosi_data: bytes
    miso_data: bytes

    def start_address(self) -> int:
        return self.address

    def data(self) -> List[int]:
        return [x for x in (self.mosi_data if self.write else self.miso_data)]


class SPISpyDevice:
    def __init__(self, device: dwf.Dwf, cs_channel: int | None = None, sck_channel: int | None = None,
                 mosi_channel: int | None = None, miso_channel: int | None = None, frequency: int = 12_500_000):
        if cs_channel is None:
            cs_channel = pin_defs.SPI_SELECT_PIN
        if sck_channel is None:
            sck_channel = pin_defs.SPI_CLOCK_PIN
        if mosi_channel is None:
            mosi_channel = pin_defs.SPI_MOSI_PIN
        if miso_channel is None:
            miso_channel = pin_defs.SPI_MISO_PIN
        self.digilent_device = dwf.DwfDigitalIn(device)
        self.cs_channel = cs_channel
        self.sck_channel = sck_channel
        self.mosi_channel = mosi_channel
        self.miso_channel = miso_channel
        self.frequency = frequency
        self.is_spying = False

    def start_spying(self):
        if self.is_spying:
            raise errors.DeviceAlreadySpyingError
        self.is_spying = True
        self.digilent_device.acquisitionModeSet(self.digilent_device.ACQMODE.RECORD)
        # sync mode
        self.digilent_device.dividerSet(-1)
        self.digilent_device.sampleFormatSet(16)
        # continuous sampling
        self.digilent_device.triggerPositionSet(-1)

        # Triggers on rising clk or rising cs channel, allowing us to separate SPI messages
        self.digilent_device.triggerSet(0, 0, ((1 << self.sck_channel) | (1 << self.cs_channel)), 0)
        self.digilent_device.configure(False, True)

    def finish_spying(self) -> List[SPIMessage]:
        if not self.is_spying:
            raise errors.DeviceNotSpyingError
        self.is_spying = False
        status = self.digilent_device.status(True)
        available, lost, corrupt = self.digilent_device.statusRecord()
        data = self.digilent_device.statusData(available)
        mosi_frames, miso_frames = self.separate_frames(data)
        return list(
            map(lambda mosi_miso: self.frame_to_spi_message(mosi_miso[0], mosi_miso[1]), zip(mosi_frames, miso_frames)))

    def separate_frames(self, data: List[int]) -> Tuple[List[List[bool]], List[List[bool]]]:
        mosi = list(map(lambda x: bool(x & (1 << self.mosi_channel)), data))
        miso = list(map(lambda x: bool(x & (1 << self.miso_channel)), data))
        cs = list(map(lambda x: bool(x & (1 << self.cs_channel)), data))
        mosi_frames, miso_frames = [], []
        mosi_bools, miso_bools = [], []
        for (mosi_bool, (miso_bool, cs_bool)) in zip(mosi, zip(miso, cs)):
            if cs_bool and len(mosi_bools) != 0:
                mosi_frames.append(mosi_bools.copy())
                miso_frames.append(miso_bools.copy())
                mosi_bools, miso_bools = [], []
                continue
            mosi_bools.append(mosi_bool)
            miso_bools.append(miso_bool)
        return mosi_frames, miso_frames

    @staticmethod
    def bools_to_data_bytes(bools: List[bool]) -> List[int]:
        data = []
        bools = list(map(lambda x: 1 if x else 0, bools))
        while len(bools) >= 8:
            bool1, bool2, bool3, bool4, *bools = bools
            bool5, bool6, bool7, bool8, *bools = bools
            data_byte = bool1 << 7 | bool2 << 6 | bool3 << 5 | bool4 << 4
            data_byte |= bool5 << 3 | bool6 << 2 | bool7 << 1 | bool8
            data.append(data_byte)
        return data

    def frame_to_spi_message(self, mosi_frame: List[bool], miso_frame: List[bool]) -> SPIMessage:
        mosi_raw = self.bools_to_data_bytes(mosi_frame)
        address, *mosi_data = mosi_raw
        miso_raw = self.bools_to_data_bytes(miso_frame)
        _, *miso_data = miso_raw
        write = False if address & 0b1000_0000 else True
        address &= 0b0111_1111
        return SPIMessage(write=write, address=address, mosi_data=mosi_data, miso_data=miso_data)
