import dwf
import time
import pin_defs
from digital_pin import DigilentDigitalOutPin, IDigitalOutPin


def press(button: IDigitalOutPin, sleep_time: float = 0.5):
    button.on()
    time.sleep(sleep_time)
    button.off()


class Buttons:
    def __init__(self, digilent_device: dwf.Dwf):
        self.sw2 = DigilentDigitalOutPin(digilent_device, pin_defs.SW2_PIN_POS, inverted=True)
        self.sw3 = DigilentDigitalOutPin(digilent_device, pin_defs.SW3_PIN_POS, inverted=True)

    def reset(self):
        self.sw2.reset()
        self.sw3.reset()
