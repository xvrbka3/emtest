from __future__ import annotations

import copy
import itertools
from typing import List

from data_packet import IAddressablePacket


class MemoryView:
    def __init__(self, raise_oob_exception: bool = True, on_oob_address: int | None = None):
        self.memory_view = {}
        self.raise_oob_exception = raise_oob_exception
        self.on_oob_address = on_oob_address

    def __getitem__(self, address: int) -> int:
        if address in self.memory_view:
            return self.memory_view[address]
        elif self.raise_oob_exception:
            raise IndexError
        return copy.copy(self.on_oob_address)

    def add_data(self, address: int, data: List[int]):
        for (i, x) in zip(itertools.count(address), data):
            self.memory_view[i] = x

    def add_data_from_packet(self, packet: IAddressablePacket):
        self.add_data(packet.start_address(), packet.data())

    def clear(self):
        self.memory_view = {}
