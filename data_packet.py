from abc import ABC, abstractmethod


class IAddressablePacket(ABC):

    @abstractmethod
    def start_address(self) -> int:
        pass

    @abstractmethod
    def data(self) -> [int]:
        pass
