import dwf


class VoltageManager:
    def __init__(self, device: dwf.Dwf):
        self.device = dwf.DwfAnalogIO(device)

    def enable_5v(self):
        self.device.channelNodeSet(0, 0, True)
        self.device.channelNodeSet(0, 1, 5.0)
        self.device.enableSet(True)
