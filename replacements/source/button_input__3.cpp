/*
 * Copyright 2016-2021 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    button_input__3.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "fsl_gpio.h"
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK66F18.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */

class RGBController {
	enum ColorSelected {
		Red = 0, Green = 1, Blue = 2,
	};
	ColorSelected selected = ColorSelected::Red;

public:

	void next() {
		int s = static_cast<int>(selected);
		++s;
		s %= 3;
		selected = static_cast<ColorSelected>(s);
	}

	void toggle() {
		switch (selected) {
		case Red:
			GPIO_PortToggle(BOARD_LED_RED_GPIO, 1U << BOARD_LED_RED_PIN);
			break;
		case Green:
			GPIO_PortToggle(BOARD_LED_GREEN_GPIO, 1U << BOARD_LED_GREEN_PIN);
			break;
		case Blue:
			GPIO_PortToggle(BOARD_LED_BLUE_GPIO, 1U << BOARD_LED_BLUE_PIN);
			break;
		}
	}
};

template<typename STATE_FUN_T>
class Debouncer {
	bool edge_passed = false;
	uint32_t positive_inputs = 0;
	const uint32_t count_needed;
	STATE_FUN_T state_fun;

public:
	Debouncer(STATE_FUN_T state_function, uint32_t bounce_time = 2) :
			count_needed(bounce_time), state_fun(state_function) {
	}

	bool edge() {
		if (get_state() && !edge_passed) {
			edge_passed = true;
			return true;
		}

		return false;
	}

	bool get_state() {
		bool pressed = state_fun();
		if (pressed && positive_inputs == count_needed) {
			return true;
		}
		if (pressed) {
			++positive_inputs;
		} else {
			edge_passed = false;
			positive_inputs = 0;
		}
		return false;
	}
};

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
int main(void) {

	/* Init board hardware. */
	BOARD_InitBootPins();
	BOARD_InitBootClocks();
	BOARD_InitBootPeripherals();
	/* Init FSL debug console. */
	BOARD_InitDebugConsole();

	RGBController controller;

	auto SW2_deb = Debouncer([](){return !GPIO_PinRead(BOARD_SW2_GPIO, BOARD_SW2_PIN);});
	auto SW3_deb = Debouncer([](){return !GPIO_PinRead(BOARD_SW3_GPIO, BOARD_SW3_PIN);});

	while (1) {
		bool SW2_press = SW2_deb.edge();
		bool SW3_press = SW3_deb.edge();
		if (SW2_press) {
			controller.next();
		}
		if (SW3_press) {
			controller.toggle();
		}
		__asm volatile ("nop");
	}
	return 0;
}
