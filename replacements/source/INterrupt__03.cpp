/*
 * Copyright 2016-2021 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    INterrupt__03.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK66F18.h"
#include "fsl_debug_console.h"
/* TODO: insert other include files here. */

bool timer_enabled = false;
bool button_pressed = false;

extern "C" void SW3_IRQHandler(void) {
	uint32_t mask = GPIO_PortGetInterruptFlags(BOARD_SW3_GPIO);
	if (mask & 1U << BOARD_SW3_PIN && !timer_enabled) {
		PIT_StartTimer(PIT_1_PERIPHERAL, kPIT_Chnl_0);
		timer_enabled = true;
	}
	GPIO_PortClearInterruptFlags(BOARD_SW3_GPIO, mask);
}

extern "C" void BOUNCE_TIMER_IRQHandler(void) {
	uint32_t mask = PIT_GetStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_0);
	PIT_StopTimer(PIT_1_PERIPHERAL, kPIT_Chnl_0);
	timer_enabled = false;
	if (!GPIO_PinRead(BOARD_SW3_GPIO, BOARD_SW3_PIN)) {
		button_pressed = true;
		
	}
	PIT_ClearStatusFlags(PIT_1_PERIPHERAL, kPIT_Chnl_0, mask);
}
/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
		if (button_pressed) {
			PRINTF("BUTTON PRESSED \r\n");
			GPIO_PortToggle(BOARD_LED_GREEN_GPIO, 1U << BOARD_LED_GREEN_PIN);
			button_pressed = false;
		}
        /* 'Dummy' NOP to allow source level single stepping of
            tight while() loop */
        __asm volatile ("nop");
    }
    return 0 ;
}
