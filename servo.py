from __future__ import annotations

import time

import dwf

import pin_defs
from digital_pin import DigilentDigitalOutPin


def map_range(x: int, from_min: int, from_max: int, to_min: int, to_max: int) -> int:
    if x < from_min:
        x = from_min
    elif x > from_max:
        x = from_max
    from_range = from_max - from_min
    to_range = to_max - to_min
    ratio = x / float(from_range)
    return int(ratio * to_range) + to_min


class Servo:
    def __init__(self, device: dwf.Dwf, pwm_channel: int | None = None, default_angle: int = 90, angle_adjustment: int = 0):
        if pwm_channel is None:
            pwm_channel = pin_defs.SERVO_PIN_POS
        self.servo_pin = DigilentDigitalOutPin(device, pwm_channel)
        self.angle = default_angle
        self.angle_adjustment = angle_adjustment
        self.set_servo_angle(default_angle)
        time.sleep(5)

    def set_raw_angle(self, angle: int, override: bool = False):
        if angle < 0 or angle > 180:
            raise AttributeError
        # Values outside of this range cause noise and potential damage to the servo
        if not override and (angle < 30 or angle > 160):
            raise AttributeError
        # 1/50 of a second = 20 ms time block
        servo_pwm_rate = 50
        clock_rate = self.servo_pin.clock_rate
        # min - 0.5 ms
        servo_pwm_min_rotation = (clock_rate / servo_pwm_rate) / 40
        # max - 2.5 ms
        servo_pwm_max_rotation = (clock_rate / servo_pwm_rate) / 8
        counter_high_ticks = map_range(angle, 0, 180, servo_pwm_min_rotation, servo_pwm_max_rotation)
        counter_low_ticks = int(clock_rate / servo_pwm_rate) - counter_high_ticks
        self.servo_pin.pwm_ticks(counter_low_ticks, counter_high_ticks)
        self.angle = angle

    def set_servo_angle(self, angle: int, override: bool = False):
        self.set_raw_angle(angle + self.angle_adjustment, override)

    def reset(self):
        if self.angle != 0:
            self.set_servo_angle(0)
            time.sleep(5)
