from dwf import _define, HDWF, c_double, _ARGIN, c_int, DwfDigitalOutIdle

# FDwfDigitalSpiFrequencySet(HDWF hdwf, double hz)
_define("FDwfDigitalSpiFrequencySet",
        (HDWF, c_double, ),
        ((_ARGIN, "hdwf"), (_ARGIN, "hz")))


# FDwfDigitalSpiClockSet(HDWF hdwf, int idxChannel)
_define("FDwfDigitalSpiClockSet",
        (HDWF, c_int,),
        ((_ARGIN, "hdwf"), (_ARGIN, "idxChannel")))


# FDwfDigitalSpiDataSet(HDWF hdwf, int idxDQ, int idxChannel)
_define("FDwfDigitalSpiDataSet",
        (HDWF, c_int, c_int,),
        ((_ARGIN, "hdwf"), (_ARGIN, "idxDQ"), (_ARGIN, "idxChannel")))


# FDwfDigitalSpiIdleSet(HDWF hdwf, int idxDQ, DwfDigitalOutIdle idle)
_define("FDwfDigitalSpiIdleSet",
        (HDWF, c_int, DwfDigitalOutIdle,),
        ((_ARGIN, "hdwf"), (_ARGIN, "idxDQ"), (_ARGIN, "idle")))


# FDwfDigitalSpiModeSet(HDWF hdwf, int iMode)
_define("FDwfDigitalSpiModeSet",
        (HDWF, c_int,),
        ((_ARGIN, "hdwf"), (_ARGIN, "iMode")))


# FDwfDigitalSpiOrderSet(HDWF hdwf, int fMSBLSB)
_define("FDwfDigitalSpiOrderSet",
        (HDWF, c_int,),
        ((_ARGIN, "hdwf"), (_ARGIN, "fMSBLSB")))
