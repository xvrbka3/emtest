import dwf

import interrupts
import loader
from analog_devices import AnalogDevices
from digital_pin import DigilentDigitalOutPin
import pin_defs
from lcd import LCDSpyDevice
from leds import Leds
from buttons import Buttons
import time
from spi_device import SPISpyDevice
import serial
import serial.tools.list_ports
from elftools.elf.elffile import ELFFile
from voltage_manager import VoltageManager
from servo import Servo


class NoMCUConnectedError:
    pass


def setup_serial_contains(device_string: str):
    ports = serial.tools.list_ports.comports()
    for port, desc, _ in ports:
        if device_string in desc:
            return serial.Serial(port, 115200, timeout=10)
    raise NoMCUConnectedError


class EmTest:
    def setup_class(self):
        if not hasattr(self, "generated_replacements"):
            self.generated_replacements = []
        if not hasattr(self, "to_delete"):
            self.to_delete = []
        self.digilent_device = dwf.Dwf()
        self.mcu_reset = DigilentDigitalOutPin(self.digilent_device, pin_defs.RESET_PIN_POS, inverted=True)
        self.mcu_reset.on()
        time.sleep(0.5)
        self.mcu_reset.off()
        time.sleep(0.5)
        self.session, self.file = loader.load_and_open_session(self.generated_replacements, self.to_delete)
        self.leds = Leds(self.digilent_device)
        self.buttons = Buttons(self.digilent_device)
        self.analog = AnalogDevices(self.digilent_device)
        self.spi = SPISpyDevice(self.digilent_device)
        self.serial = setup_serial_contains('DAPLink')
        self.uart = setup_serial_contains('UART')
        self.multiplexer = DigilentDigitalOutPin(self.digilent_device, pin_defs.MULTIPLEXER_PIN_POS)
        self.lcd = LCDSpyDevice(self.digilent_device)
        self.elf_file = ELFFile(self.file)
        self.interrupts = interrupts.Interrupts(self.session.target, self.elf_file)
        self.voltage_manager = VoltageManager(self.digilent_device)
        self.voltage_manager.enable_5v()
        self.servo = Servo(self.digilent_device, angle_adjustment=45)

    def teardown_class(self):
        self.session.target.reset_and_halt()
        self.session.close()
        self.digilent_device.close()
        self.serial.close()
        self.uart.close()
        self.file.close()

    def reset_peripherals(self):
        self.buttons.reset()
        self.serial.reset_input_buffer()
        self.multiplexer.reset()
        self.lcd.reset()

    def setup_method(self, method: str):
        self.session.target.reset_and_halt()
        self.reset_peripherals()
        self.session.target.resume()
        time.sleep(0.1)
