#!/bin/zsh

cd "$EMTEST_FOLDER" || return 3

function run_test() {
  python3 -m pytest -v --junitxml=report.xml $1
  RES=$?
  cp /emtest/report.xml $CI_PROJECT_DIR
  return $RES
}

case ${CI_COMMIT_TAG[1,-3]} in
"Submission_01")
  run_test weeks/test_empty.py
  return $?
  ;;
"Submission_02")
  run_test weeks/test_gpio.py
  return $?
  ;;
"Submission_03")
  run_test weeks/test_interrupt.py
  return $?
  ;;
"Submission_04")
  run_test weeks/test_hsv.py
  return $?
  ;;
"Submission_05")
  run_test weeks/test_adc.py
  return $?
  ;;
"Submission_06")
  run_test weeks/test_spi.py
  return $?
  ;;
"Submission_07")
  run_test weeks/test_i2c.py
  return $?
  ;;
"Submission_08")
  run_test weeks/test_uart.py
  return $?
  ;;
"Submission_09")
  echo "Test does not exist, make sure its right :)"
  return 2
  ;;
"Submission_10")
  run_test weeks/test_lcd.py
  return $?
  ;;
"Submission_11")
  run_test weeks/test_network.py
  return $?
  ;;
*)
  echo "Invalid tag"
  return 1
esac
