import subprocess
from typing import Optional, List, Tuple, BinaryIO

from pyocd.core.helpers import ConnectHelper
from pyocd.core.session import Session
from pyocd.flash.file_programmer import FileProgrammer

import repository
from dotenv import load_dotenv
import os
import shutil
import logging
import re


# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def copy_files(src, dst):
    files = os.listdir(src)
    for filename in files:
        shutil.copy2(os.path.join(src, filename), dst)


def flash(session: Session, project_name: str, filename: str):
    repo_folder = get_repository_folder()
    filepath = f'{repo_folder}/{project_name}/debug/{filename}'
    logging.debug(f'Flashing file: {filepath}')
    FileProgrammer(session).program(filepath)


class FailedSession(BaseException):
    pass


class EnvironmentVariableNotSet(BaseException):
    pass


def copy_replacements(filepath: str, generated_replacements: List[Tuple[str, str]]):
    for path, file_string in generated_replacements:
        with open(f'{filepath}/{path}', 'w') as file:
            file.write(file_string)


def get_repository_folder() -> str:
    if os.getenv("CI") is None:
        return "../repo"
    return os.getenv("CI_PROJECT_DIR")


def find_project_folder(path: str) -> Optional[str]:
    project_folders = []
    for root, _, files in os.walk(path):
        if any(map(lambda file: re.fullmatch(r'.*\.mex$', file), files)):
            print(root[len(path):])
            project_folders.append(root[len(path):])
    if len(project_folders) > 1:
        raise RuntimeError("Multiple projects found")
    elif len(project_folders) == 1:
        return project_folders[0]
    return None


def delete_folders(to_delete: [str], path: str):
    for folder in to_delete:
        try:
            shutil.rmtree(f'{path}/{folder}')
        except FileNotFoundError:
            pass


def load_repository(week_branch: str):
    username = os.getenv('USERNAME')
    password = os.getenv('PASSWORD')
    year = os.getenv('YEAR')
    student_id = os.getenv('STUDENT_ID')
    repo_folder = get_repository_folder()
    try:
        shutil.rmtree(repo_folder)
    except FileNotFoundError:
        pass
    url = f'https://{username}:{password}@gitlab.fi.muni.cz/pv198/{year}/{student_id}.git'
    repo = repository.download_repo(url, get_repository_folder())
    repo.git.checkout(week_branch)


def prepare_build_files(gen_replacements: List[Tuple[str, str]], to_delete: List[str]) -> str:
    emtest_folder = get_emtest_folder()
    repo_folder = get_repository_folder()
    folder_name = find_project_folder(repo_folder)
    if folder_name is None:
        print(f'{emtest_folder}, {repo_folder}, {folder_name}')
        raise FileNotFoundError
    copy_files(f'{emtest_folder}/build_files', f'{repo_folder}/{folder_name}/')
    copy_replacements(f'{repo_folder}/{folder_name}', gen_replacements)
    delete_folders(to_delete, f'{repo_folder}/{folder_name}')
    return folder_name


def build_binary(repository_folder: str, folder_name: str):
    print(os.path.abspath(os.curdir))
    subprocess.run(['./build_debug_cpp.sh'], cwd=f'{repository_folder}/{folder_name}/', check=True)


def get_emtest_folder():
    if os.getenv("EMTEST_FOLDER") is None:
        raise EnvironmentVariableNotSet
    return os.getenv("EMTEST_FOLDER")


def handle_session(gen_replacements: List[Tuple[str, str]], to_delete: List[str]) -> Tuple[Session, BinaryIO]:
    repo_folder = get_repository_folder()
    folder_name = prepare_build_files(gen_replacements, to_delete)
    build_binary(repo_folder, folder_name)
    # Auto open does not work for some odd reason
    session = ConnectHelper.session_with_chosen_probe(return_first=True, auto_open=False)
    if session is None:
        raise FailedSession
    session.open()
    flash(session, folder_name, 'StudentTest.elf')
    return session, open(f'{repo_folder}/{folder_name}/debug/StudentTest.elf', 'rb')


def load_and_open_session(gen_replacements=None, to_delete=None) -> Tuple[Session, BinaryIO]:
    if gen_replacements is None:
        gen_replacements = []
    if to_delete is None:
        to_delete = []
    load_dotenv()
    if os.getenv("OVERRIDE_BRANCH"):
        load_repository(os.getenv("OVERRIDE_BRANCH"))
    return handle_session(gen_replacements, to_delete)


# ARMGCC_DIR=/home/vrbkam/Downloads/gcc-arm-none-eabi-10.3-2021.10/ ./build_debug_cpp.sh

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
