from typing import List

import dwf


class AnalogOutPin:
    def __init__(self, channel: int, digilent_device: dwf.Dwf):
        self.digilent_device = dwf.DwfAnalogOut(digilent_device)
        self.channel = channel

    def set_data_waveform(self, waveform: List[float], frequency: int = 10e3, amplitude: float = 3.3):
        # Prevent potential damage to the MCU, voltage is calculated via amplitude * waveform
        for i in waveform:
            if amplitude * i < -0.01 or amplitude * i > 3.5:
                raise AttributeError
        # Enables the channel and AM/FM modulation
        self.digilent_device.nodeEnableSet(self.channel, self.digilent_device.NODE.CARRIER, True)
        # Sets the generation output function, in our case, it is custom based on the data
        self.digilent_device.nodeFunctionSet(self.channel, self.digilent_device.NODE.CARRIER, self.digilent_device.FUNC.CUSTOM)
        # Sets the waveform to iterate trough, in most cases, this will be a single value
        self.digilent_device.nodeDataSet(self.channel, self.digilent_device.NODE.CARRIER, waveform)
        # Sets frequency and amplitude, frequency does not matter if there is only a single value
        self.digilent_device.nodeFrequencySet(self.channel, self.digilent_device.NODE.CARRIER, frequency)
        self.digilent_device.nodeAmplitudeSet(self.channel, self.digilent_device.NODE.CARRIER, amplitude)
        # Confirms the configuration
        self.digilent_device.configure(self.channel, True)

    def set_value(self, value: float):
        self.set_data_waveform([value])
