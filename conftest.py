import pytest

from emtest import EmTest


@pytest.fixture(scope="session")
def emtest_session() -> EmTest:
    emtest = EmTest()
    emtest.setup_class()
    yield emtest
    emtest.teardown_class()


@pytest.fixture
def emtest(request: pytest.FixtureRequest, emtest_session: EmTest) -> EmTest:
    if hasattr(request, "param") and isinstance(request.param, bool):
        emtest_session.multiplexer.inverted = request.param
    emtest_session.setup_method("empty")
    return emtest_session
