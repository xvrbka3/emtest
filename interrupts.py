from typing import List

from elftools.elf.elffile import ELFFile
from pyocd.core.target import Target
import pyelftools.dwarf_decode_address
import vtable_constants


class InterruptGroup:
    def __init__(self, mcu_target: Target, elf_file: ELFFile, vtable_positions: List[int]):
        self.mcu_target = mcu_target
        self.elf_file = elf_file
        self.vtable_positions = vtable_positions

    def interrupt_not_default(self, vector_table_number: int) -> bool:
        vector_addr = self.mcu_target.read32(vector_table_number * 4)
        _, filename, _ = pyelftools.dwarf_decode_address.process_file(self.elf_file, vector_addr)
        return 'startup' not in filename

    def has_any_interrupt_set(self) -> bool:
        return any(map(lambda pos: self.interrupt_not_default(pos), self.vtable_positions))


class Interrupts:
    def __init__(self, mcu_target: Target, elf_file: ELFFile):
        self.pit = InterruptGroup(mcu_target, elf_file, vtable_constants.PIT_VEC_POSITIONS)
        self.gpio = InterruptGroup(mcu_target, elf_file, vtable_constants.GPIO_VEC_POSITIONS)
